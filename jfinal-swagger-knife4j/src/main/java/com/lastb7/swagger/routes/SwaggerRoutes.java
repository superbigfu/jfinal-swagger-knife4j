package com.lastb7.swagger.routes;

import com.jfinal.config.Routes;
import com.jfinal.kit.PropKit;
import com.lastb7.swagger.controller.SwaggerController;

/**
 * swagger route
 *
 * @author: lbq
 * 联系方式: 526509994@qq.com
 * 创建日期: 2020/6/12
 */
public class SwaggerRoutes extends Routes {

    @Override
    public void config() {
        boolean enable = PropKit.use("swagger.properties").getBoolean("enable", false);
        if (enable) {
            add("/swagger", SwaggerController.class);
        }
    }
}
