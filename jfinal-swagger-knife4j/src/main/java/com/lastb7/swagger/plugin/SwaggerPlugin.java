package com.lastb7.swagger.plugin;

import java.util.Map;

import com.jfinal.kit.PropKit;
import com.jfinal.plugin.IPlugin;
import com.jfinal.template.Engine;
import com.lastb7.swagger.common.SwaggerHttpCode;
import com.lastb7.swagger.common.SwaggerRes;
import com.lastb7.swagger.common.SwaggerConst;

/**
 * @author: lbq
 * 联系方式: 526509994@qq.com
 * 创建日期: 2020/6/12
 */
public class SwaggerPlugin implements IPlugin {
    /**
     * swagger配置文件
     */
    private String propPath;

    /**
     * http返回状态
     */
    private Map httpCode;

    /**
     * http返回状态200时的通用返回格式
     */
    private Class<?> commonRet;

    public SwaggerPlugin setPropPath(String propPath) {
        this.propPath = propPath;
        return this;
    }

    public SwaggerPlugin setHttpCode(Map httpCode) {
        this.httpCode = httpCode;
        return this;
    }

    public SwaggerPlugin setCommonRet(Class<?> commonRet) {
        this.commonRet = commonRet;
        return this;
    }

    public SwaggerPlugin() {
        this.propPath = "swagger.properties";
        this.httpCode = new SwaggerHttpCode().getHttpCodeKv();
        this.commonRet = SwaggerRes.class;
    }

    @Override
    public boolean start() {
        SwaggerConst.CONFIG = PropKit.use(this.propPath);
        SwaggerConst.HTTP_CODE = this.httpCode;
        SwaggerConst.COMMON_RES = this.commonRet;
        SwaggerConst.RESPONSE_IN_DATA = PropKit.use(this.propPath).getBoolean("responseInData",true);

        Engine engine = Engine.create("swagger");
        engine.setDevMode(false);
        engine.setBaseTemplatePath("/swagger-template");
        engine.setToClassPathSourceFactory();

        return true;
    }

    @Override
    public boolean stop() {
        return true;
    }
}
